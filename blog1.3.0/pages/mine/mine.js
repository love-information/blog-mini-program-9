// pages/mine/mine.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //初始化数据
    code:'',
    input1:"",
    input2:"",
    input3:"",
    hiddenmodalput: true,
    modalHidden: true,
    hideNotice: false,
    notice: '🔈 欢迎来到玖玖博客,风里雨里,我们等你~',
    motto: 'Hello 欢迎来到玖玖博客',
    userInfo: {
      nickName:"",
      avatarUrl:'',//微信用户头像的https地址
    },
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: false//wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },

    // 事件处理函数
    bindViewTap() {
      wx.navigateTo({
        url: '../logs/logs'
      })
    },

    //点击查看收藏


    // onLoad() {
      // if (wx.getUserProfile) {
      //   this.setData({
      //     canIUseGetUserProfile: true
      //   })
      // }
    // },
 
    getUserProfile(e) {
      let _that=this;
      // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
      wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          // console.log(res)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
          //将登录信息添加到全局变量
          app.appData.userinfo=res.userInfo

          wx.login({
            success: (res) => {          
              if(res.code){
                wx.request({
                        url: 'https://linshiqiang.cn/api/login/logincheck',
                        data: {
                                code: res.code,
                                user_name:_that.data.userInfo.nickName,
                                user_img:_that.data.userInfo.avatarUrl,
                        },
                        method:'GET',
                        success: function(res) {                               
                                        console.log("微信用户登录成功");  
                                        // console.log(res)                             
                        }
                       
                    })
                }
              }
            })

        }

      })
    },  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options){
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
    this.setData({
      userInfo:app.appData.userinfo
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo:app.appData.userinfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享----------------------------------------------------------
   */
  onShareAppMessage: function () {
    Page({
      onShareAppMessage() {
        const promise = new Promise(resolve => {
          setTimeout(() => {
            resolve({
              title: '玖玖博客'
            })
          }, 2000)
        })
        return {
          title: '玖玖博客',
          path: '/pages/mine/mine',
          promise 
        }
      }
    })
  },
//用户点击右上角分享朋友圈
	onShareTimeline: function () {
		return {
        title: '玖玖博客',
        path: '/pages/mine/mine',
	      query: {
	        key: value
	      },
	    }
	},
  //下拉刷新---------------------------------------
  onPullDownRefresh:function()
  {
    let that = this;
    this.onLoad(this.options)
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function()
    {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
      wx.showToast({
        title: '刷新成功',
        icon: 'success',
        duration: 2000//持续的时间
      })
    },1000); 
  },
  //退出登录功能-----------------------------------------------------
  reload:function(){
    let _this=this;
    if(_this.data.userInfo.nickName == "请点击头像登录"){
      wx.showToast({
        title: '您尚未登录',
        icon: 'none',  
        duration: 2000//持续的时间
      })
      return false;
    }
    wx.showModal({
      title: '确认提示',
      content: '是否退出登录',
      success: function (res) { 
        if (res.confirm) {//这里是点击了确定以后
          wx.reLaunch({
            url: '../mine/mine',
            success: (res) => {
              app.appData.userinfo={
                nickName:"请点击头像登录",
                avatarUrl:'../../images/user.png',
              }
              wx.showToast({
                title: '退出登录成功',
                icon: 'success',
                duration: 2000//持续的时间
              })
            },
            fail: (res) => {
              wx.showToast({
                title: '退出登录失败',
                icon: 'fail',
                duration: 2000//持续的时间
              })
            },
            complete: (res) => {},
          })
        } else {//这里是点击了取消以后 
          wx.showToast({
            title: '取消成功',
            icon: 'fail',
            duration: 2000//持续的时间
          })
        }
      }
    })
  },
//----------------------------------------------
//联系作者
    getadmin:function(){
      this.setData({
        modalHidden: false
        })
    },  
    /**
   * 点击取消
   */
  modalCandel: function() {
    // do something
    this.setData({
         modalHidden: true
    })
    }, 
    /**
      *  点击确认
      */
     modalConfirm: function() {
    // do something
    this.setData({
         modalHidden: true
    })
    },
//--------------------------------------------------
//长按保存图片
baocun: function (e) {
  wx.showModal({
      title: '提示',
      content: '点击确定保存二维码',
      success: function (res) {
          if (res.confirm) {
              console.log('用户点击确定')
              wx.getImageInfo({
                  src: '../../images/we.jpg',
                  success: function (res) {
                      // console.log(res);
                      var path = res.path;
                      wx.saveImageToPhotosAlbum({
                          filePath: path,
                          success: function (res) {
                              console.log('图片已保存');
                          },
                          fail: function (res) {
                              console.log('保存失败');
                          }
                      })
                  }
              });
          } else if (res.cancel) {
              console.log('用户点击取消')
          }
      }
  })
},
//---------------------------
//意见反馈
userinput:function(){
  this.setData({
    hiddenmodalput: !this.data.hiddenmodalput    
    })    
},
//取消按钮
cancel: function () {
  this.setData({ 
  hiddenmodalput: true 
  });  
  },  
  //确认  
  confirm: function () { 
    wx.showToast({
      title: '意见反馈成功',
      icon: 'success',
      duration: 2000//持续的时间
    }) 
  this.setData({  
  hiddenmodalput: true ,  
  input1:"",
  input2:"",
  input3:"",
  })   
  },
  
//-------------------------------
})