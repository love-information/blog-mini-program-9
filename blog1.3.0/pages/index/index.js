// pages/category/category.js
var util = require('../../utils/util.js'); //引入时间 
Page({

  /**
   * 页面的初始数据
   */
  data: {

    go_top_show: false,

    articleList: {},
    timestampConvert: [], //时间戳转换成日期
    time: '',
    hiddenmodalput: true,
    modalHidden: true,
    hideNotice: false,
    notice: '🔈 欢迎来到玖玖博客,风里雨里,我们等你~',
    motto: 'Hello 欢迎来到玖玖博客',
    hasMore: true,
    hasRefesh: false,
  },

  //过滤器
  // formatTime : function(now) {
  // var time = new Date( now*1000 );
  // var year=time.getFullYear();  //取得4位数的年份
  // var month=(time.getMonth()+1).toString().padStart(2,"0");  //取得日期中的月份，其中0表示1月，11表示12月
  // var date=time.getDate().toString().padStart(2,"0");      //返回日期月份中的天数（1到31）
  // var hour=time.getHours().toString().padStart(2,"0");     //返回日期中的小时数（0到23）
  // var minute=time.getMinutes().toString().padStart(2,"0"); //返回日期中的分钟数（0到59）
  // // var second=time.getSeconds().toString().padStart(2,"0"); //返回日期中的秒数（0到59）
  // return year + "-"+ month + "-" + date + " " + hour + ":" + minute
  // },

  //获取文章数据
  getArticleDate: function () {
    let that = this
    wx.request({
      url: 'https://linshiqiang.cn/api/article/list',
      data: {
        start: this.data.articleList.length,
        count: 5
      },
      success: function (res) {
        //  console.log(res)
        let articleList = res.data.data.articleList
        let likeList = res.data.data.likeList
        let commentList = res.data.data.commentList
        // console.log(articleList)
        // console.log(commentList)

        var temp = []
        temp = articleList
        for (var i = 0; i < articleList.length; i++) {
          for (var j = 0; j < likeList.length; j++) {
            articleList[i]["likeNum"] = likeList[i]
          }
        }

        var temp2 = []
        temp2 = articleList
        for (var i = 0; i < articleList.length; i++) {
          for (var j = 0; j < commentList.length; j++) {
            articleList[i]["commentNum"] = commentList[i]
          }
        }

        that.setData({
          articleList: articleList,
          hasMore: false,
        })
      }
    })

  },

  //获取分类数据
  getCategoryDate: function () {
    let that = this
    wx.request({
      url: 'https://linshiqiang.cn/api/category/list',
      success: function (res) {
        // console.log(res)
        that.setData({
          categoryList: res.data.data.categoryList
        })
        //  console.log(res.data.data.categoryList)
        //  console.log(res.data.data.categoryList[0].category_name)
      },
      fail: function (err) {
        //  console.log(err)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 调用函数时，传入new Date()参数，返回值是日期和时间
    var time = util.formatTime(new Date());
    // 再通过setData更改Page()里面的data，动态更新页面的数据
    this.setData({
      time: time,
    });

    //调用文章
    this.getArticleDate()
    this.getCategoryDate() //调用分类

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  /**
   * 用户点击右上角分享--------------------------------------------------------
   */
  onShareAppMessage: function () {
    Page({
      onShareAppMessage() {
        const promise = new Promise(resolve => {
          setTimeout(() => {
            resolve({
              title: '玖玖博客'
            })
          }, 2000)
        })
        return {
          title: '玖玖博客',
          path: '/pages/index/index',
          promise
        }
      }
    })
  },
  //用户点击右上角分享朋友圈
  onShareTimeline: function () {
    return {
      title: '玖玖博客',
      path: '/pages/index/index',
      query: {
        key: value
      },
    }
  },


  //下拉刷新---------------------------------------------------------------
  onPullDownRefresh: function () {
    let that = this;
    //刷新页面方法
    // 1.重置
    this.setData({
      goodsList: []
    })
    this.getArticleDate()
    //停止下拉
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function () {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
      wx.showToast({
        title: '刷新成功',
        icon: 'success',
        duration: 2000 //持续的时间
      })
    }, 1000);




    //  下拉刷新时  显示实时时间
    setTimeout(function () {
      // 调用函数时，传入new Date()参数，返回值是日期和时间
      var time = util.formatTime(new Date());
      // 再通过setData更改Page()里面的data，动态更新页面的数据
      that.setData({
        time: time,
      });
    }, 1000);
  },

  // 获取滚动条当前位置
  onPageScroll: function (e) {
    // console.log(e)
    if (e.scrollTop > 100) {
      this.setData({
        go_top_show: true
      })
    } else {
      this.setData({
        go_top_show: false
      })
    }
  },
  // 回到顶部
  goTop: function (e) {
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  }

})