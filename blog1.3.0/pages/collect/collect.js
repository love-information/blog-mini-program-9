// pages/collect/collect.js
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tips:'',
    collectLike:{}

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options.user_name)  //传递微信名称
    // console.log("-----",app.appData.userinfo.nickName)
    if(app.appData.userinfo.nickName == '请点击头像登录' || app.appData.userinfo.nickName == undefined){
      this.setData({
        tips:"请登录之后查看收藏",
      })
      return false;
    }


 //获取到收藏列表的数据
   let that =this
   console.log(app.appData.userinfo.nickName)
 wx.request({
  url: 'https://linshiqiang.cn/api/article/collectlists?user_name='+app.appData.userinfo.nickName, //这里写后台文章接口的url',
  success: function (res) {
    console.log(res)

    let articleList = res.data.data.articleList
    let likeList = res.data.data.likeList
    let commentList = res.data.data.commentList


    var temp = []
    temp = articleList
    for (var i = 0; i < articleList.length; i++) {
      for (var j = 0; j < likeList.length; j++) {
        articleList[i]["likeNum"] = likeList[i]
      }
    }

    var temp2 = []
    temp2 = articleList
    for (var i = 0; i < articleList.length; i++) {
      for (var j = 0; j < commentList.length; j++) {
        articleList[i]["commentNum"] = commentList[i]
      }
    }


    that.setData({
      collectLike:res.data.data.articleList
    })

 
   
  }
})

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
     //下拉刷新---------------------------------------------------------------
     onPullDownRefresh: function () {
      let that = this;
      //刷新页面方法
      // 1.重置
      this.setData({
        goodsList: []
      })
      this.onLoad(this.options)
      //停止下拉
      wx.showNavigationBarLoading() //在标题栏中显示加载
      //模拟加载
      setTimeout(function () {
        // complete
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
        wx.showToast({
          title: '刷新成功',
          icon: 'success',
          duration: 2000 //持续的时间
        })
      }, 1000);
  
  
    },
})