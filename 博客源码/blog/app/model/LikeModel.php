<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 15:31
 */

namespace app\model;

use think\Model;

class LikeModel extends Model
{
    protected $name = 'like'; // 表名
    protected $pk = 'like_id'; // 主键
}