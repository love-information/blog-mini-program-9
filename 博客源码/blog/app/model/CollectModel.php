<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 15:31
 */

namespace app\model;

use think\Model;

class CollectModel extends Model
{
    protected $name = 'collect'; // 表名
    protected $pk = 'collect_id'; // 主键

    public static function encrypt($password)
    {
        $salt = "0w37qVYCCu#SV7s4x3ctMNwQS@&4Hc";
        $password = md5($salt . md5($salt . $password . $salt) . $salt);
        return $password;
    }

    public static function checkPassword($password, $encryptPassword)
    {
        return self::encrypt($password) == $encryptPassword;
    }
}