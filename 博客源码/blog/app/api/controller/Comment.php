<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\UserModel;
use app\model\CommentModel;
use app\model\ArticleModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    // 评论增加
    public function add()
    {
        // 1. 获取参数
        // 2. 校验参数
        // 3. 验证文本是否合规。
        // 4. 写入数据库
        // 5. 返回数据
        
        // $nickname='游客'.rand(100,999);
        
        $params = Request::param();
        $userName=Request::param('user_name');
        $articleId=Request::param('article_id');

        
        $validate = Validate::rule([
            'comment_content' => 'require|min:2|max:500',
            'user_name' => 'require|min:1|max:20',
            'user_img' => 'require|min:1|max:1024',
            'article_id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check($params)) {
            $data = [
                'status' => 10002,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }

        //调用百度api接口，验证内容是否合规
        require_once '../extend/baiduapi/AipContentCensor.php';
        $client = new \AipContentCensor(
            24166875, "3tSrNYamKP4RGj825HMWefGI", "em2ATpwdWeabbbTrRY5EOmNAteINaSZ3");

        $result = $client->textCensorUserDefined($params['comment_content']);
        //包含
        if ($result['conclusionType'] == 2) {
            $data = [
                'status' => 10003,
                'message' => "评论内容包含敏感词",
                'data' => []
            ];
            return json($data);
        }
        //疑似
        if ($result['conclusionType'] == 3) {
            $params['update_time'] = time();
            $params['add_time'] = time();
            CommentModel::create($params);
            $data = [
                'status' => 10003,
                'message' => "评论内容疑似包含敏感词",
                'data' => []
            ];
            return json($data);
        }

        $insertData = $params;
        $insertData['add_time'] =  time();
        
        
        
        $userId = UserModel::where("user_name","=",$userName)->value("user_id");
        $insertData['user_id']=$userId;
        
        
        $result = CommentModel::create($insertData);
        
        
        if ($result) {
        $counts = ArticleModel::where("article_id","=",$articleId)->find();//查找的文章
        $counts['counts']+=1;
        $a=$counts->save();
        
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'result' => true,
                    'comment' => $result->toArray(),
                ]
            ];
            return json($data);
        } else {
            $data = [
                'status' => 1,
                'message' => '',
                'data' => [
                    'result' => false,
                    'comment' => $result->toArray()
                ]
            ];
            return json($data);
        }
    }

    // 评论列表
    public function list()
    {
        // 接收article_id参数
        // 根据article_id获取到评论列表
        $articleId = Request::param("article_id");
        $validate = Validate::rule([
            'article_id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            $data = [
                'status' => 10002,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }

        // 从数据库读取出来
        $result = CommentModel::where("article_id", "=", $articleId)
            ->where("status","=",1)
            ->order("add_time", 'desc')
            ->select();
            
        $commentList = [];
        foreach ($result as $item) {
          $temp = $item->toArray(); 
              $commentList[] = [
                'comment_id' => $temp['comment_id'],
                'comment_content' => $temp['comment_content'],
                'user_name' => $temp['user_name'],
                'user_img' =>$temp['user_img'],
                'user_id' =>$temp['user_id'],
                'article_id'=>$temp['article_id'],
                'add_time' => $temp['add_time'],
            ];
        }

        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'commentList' => $commentList
            ]
        ];
        return json($data);
    }
}
