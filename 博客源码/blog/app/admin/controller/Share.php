<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\ShareModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Share
{
    public function index()
    {
        $shareList = ShareModel::select();

        View::assign('shareList', $shareList);
        return View::fetch();
    }
    public function delete()
    {
        $shareId = Request::param("share_id");
        $validate = Validate::rule([
            'share_id|分享id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['share_id' => $shareId])) {
            echo $validate->getError();
            exit();
        }

        $result = ShareModel::destroy($shareId);
        return View::fetch('public/tips', ['result' => $result]);
    }
}
