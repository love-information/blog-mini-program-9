<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\LikeModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Like
{
    public function index()
    {
        $likeList = LikeModel::select();

        View::assign('likeList', $likeList);
        return View::fetch();
    }
    public function delete()
    {
        $likeId = Request::param("like_id");
        $validate = Validate::rule([
            'like_id|点赞id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['like_id' => $likeId])) {
            echo $validate->getError();
            exit();
        }

        $result = LikeModel::destroy($likeId);
        return View::fetch('public/tips', ['result' => $result]);
    }
}
