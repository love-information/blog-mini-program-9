<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

/**
 * 写功能的步骤：
 * 1. 视图先渲染出来，确定需要什么数据
 * 2. 获取参数，校验参数（可能没有）
 * 3. 使用Model处理（查询、写操作）
 * 4. 数据层model处理完成之后，把结果传递视图层view显示。
 *
 * Class Comment
 * @package app\admin\controller
 */
class Comment
{
    // 列表
    public function index()
    {
        $status = Request::param("status", 1);

        // 查找列表数据
        $commentList = CommentModel::where('status', '=', $status)->select();

        View::assign('commentList', $commentList);
        View::assign('status', $status);
        return View::fetch();
    }

    // 表示把评论标记为垃圾评论，移入回收站
    public function garbage()
    {
        // 1. 获取评论id
        // 2. 修改评论状态status = 2就可以了。
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $comment = CommentModel::find($commentId);
        $comment['status'] = 2;
        $result = $comment->save();

        $data = [
            'status' => $result ? 0 : 10001,
            'message' => $result ? '' : '修改数据库失败',
            'data' => [
                'result' =>$result ? true : false
            ],
        ];
        return json($data);
    }

    // 恢复
    public function resume()
    {
        // 1. 获取评论id
        // 2. 修改评论状态status = 2就可以了。
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $comment = CommentModel::find($commentId);
        $comment['status'] = 1;
        $result = $comment->save();

        $data = [
            'status' => $result ? 0 : 10001,
            'message' => $result ? '' : '修改数据库失败',
            'data' => [
                'result' =>$result ? true : false
            ],
        ];
        return json($data);
    }

    // 评论删除
    public function delete()
    {
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $result = CommentModel::destroy($commentId);

        $data = [
            'status' => $result ? 0 : 10001,
            'message' => $result ? '' : '修改数据库失败',
            'data' => [
                'result' =>$result ? true : false
            ],
        ];
        return json($data);
    }
}
