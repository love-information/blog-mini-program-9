<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\CollectModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Collect
{
    public function index()
    {
        $collectList = CollectModel::select();

        View::assign('collectList', $collectList);
        return View::fetch();
    }
    public function delete()
    {
        $collectId = Request::param("collect_id");
        $validate = Validate::rule([
            'collect_id|收藏id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['collect_id' => $collectId])) {
            echo $validate->getError();
            exit();
        }

        $result = CollectModel::destroy($collectId);
        return View::fetch('public/tips', ['result' => $result]);
    }
}
