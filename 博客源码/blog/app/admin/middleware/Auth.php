<?php

namespace app\admin\middleware;

use think\Response;

class Auth
{
    public function handle($request, \Closure $next)
    {
        // 如果不是login相关的页面。
        if (!preg_match("/login/", $request->pathinfo())) {
            // 检测是否有登录，如果没有登录，调整到登录页面
            if (empty(session("admin_email"))) {
                return redirect('/index.php?s=admin/login/index');
            }
        }
        $response = $next($request);
        return $response;
    }

    public function end(Response $response)
    {

    }
}