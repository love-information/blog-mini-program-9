// pages/category/category.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
      articleList:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      // this.getCategoryDate()
        // console.log("分类ID",options.category_id)
        let that = this
        wx.request({
          url: 'https://linshiqiang.cn/api/category/list',
          success: function (res) {
            let categoryList = res.data.data.categoryList
            // console.log(categoryName)
            for (let i = 0; i < categoryList.length; i++) {
              if (options.category_id == categoryList[i].category_id) {
                // console.log(categoryName[i])
                that.setData({
                  categoryName: categoryList[i]
                })
              }
            }
          }
        })

        wx.request({
          url: 'https://linshiqiang.cn/api/article/list?category_id='+options.category_id,
          success:function(res){
            // console.log("文章里对应的分类数据",res)
            let articleList = res.data.data.articleList
            let likeList = res.data.data.likeList
            let commentList = res.data.data.commentList
            var temp = []
            temp = articleList
            for (var i = 0; i < articleList.length; i++) {
              for (var j = 0; j < likeList.length; j++) {
                articleList[i]["likeNum"] = likeList[i]
              }
            }
            var temp2 = []
            temp2 = articleList
            for (var i = 0; i < articleList.length; i++) {
              for (var j = 0; j < commentList.length; j++) {
                articleList[i]["commentNum"] = commentList[i]
              }
            }
            that.setData({
              articleList: articleList,
              // hasMore: false,
            })
              // console.log("文章里对应的分类数据",articleList)
          }
        })


        

    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

     //下拉刷新---------------------------------------------------------------
  onPullDownRefresh: function () {
    let that = this;
    //刷新页面方法
    // 1.重置
    this.setData({
      goodsList: []
    })
    this.onLoad(this.options)
    //停止下拉
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function () {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
      wx.showToast({
        title: '刷新成功',
        icon: 'success',
        duration: 2000 //持续的时间
      })
    }, 1000);


  },
})