// pages/classify/classify.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryList:{}
  },

    //点击跳转到详情页面
    // categoryDetail:function(){
    //   wx.navigateTo({
    //     url: '../detail/detail',
      
    //   })
    // },


      //获取分类数据
  getCategoryDate:function(){
    let that = this
    wx.request({
     url: 'https://linshiqiang.cn/api/category/list',
     success:function(res){
      //  console.log(res)
       that.setData({
         categoryList:res.data.data.categoryList
       })
      //  console.log(res.data.data.categoryList)
      //  console.log(res.data.data.categoryList[0].category_name)
     },
     fail:function(err){
       console.log(err)
     }
   })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getCategoryDate()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  /**
   * 用户点击右上角分享-------------------------------------------------------------------
   */
  onShareAppMessage: function () {
    Page({
      onShareAppMessage() {
        const promise = new Promise(resolve => {
          setTimeout(() => {
            resolve({
              title: '玖玖博客'
            })
          }, 2000)
        })
        return {
          title: '玖玖博客',
          path: '/pages/classify/classify',
          promise 
        }
      }
    })
  },
//用户点击右上角分享朋友圈
	onShareTimeline: function () {
		return {
        title: '玖玖博客',
        path: '/pages/classify/classify',
	      query: {
	        key: value
	      },
	    }
	},
  //下拉刷新--------------------------------------------------------------------
  onPullDownRefresh:function()
  {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function()
    {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
      wx.showToast({
        title: '刷新成功',
        icon: 'success',
        duration: 2000//持续的时间
      })
    },1000); 
  },
})